<?php

define ('PUBLIC_DIR', "/public");

if(!empty($_GET) && !empty($_GET['page'])) {
    $template_file = __DIR__."/templates/{$_GET['page']}.php";
    if(file_exists($template_file)) {
        require_once $template_file;
    } else {

        require_once __DIR__."/templates/404.php";
    }

    
} else {
    require_once __DIR__."/templates/Home.php";
}

